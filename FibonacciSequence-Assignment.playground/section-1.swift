// Thinkful Playground
// Thinkful.com

// Fibonacci Sequence

// By definition, the first two numbers in the Fibonacci sequence are 1 and 1, or 0 and 1, depending on the chosen starting point of the sequence, and each subsequent number is the sum of the previous two.

import UIKit

class FibonacciSequence {
    
    let includesZero: Bool
    let values: [UInt]
    
    init(maxNumber: UInt, includesZero: Bool) {
        self.includesZero = includesZero
        
        var sequence : [UInt]
        
        if includesZero == true {
            sequence = [0,1]
            } else {
            sequence = [1,1]
        }
        
        while sequence.last <= maxNumber {
            let (lastNumber, didOveflow) = UInt.addWithOverflow(sequence[sequence.count - 1], sequence[sequence.count - 2])
            if didOveflow == true {
                println("overFlow happened!")
                break
            }
            sequence.append(lastNumber)
            }
        
        sequence.removeLast()
        values = sequence
        }
    
    init(numberOfItemsInSequence: Int, includesZero: Bool) {
    
        self.includesZero = includesZero
        
        var sequence : [UInt]
        
        if includesZero == true {
            sequence = [0,1]
        } else {
            sequence = [1,1]
        }
        let lastNumber = sequence.last
        let secondToLastNumber = sequence[sequence.count - 1]
        while sequence.count <= numberOfItemsInSequence {
            let (nextNumber, didOveflow) = UInt.addWithOverflow(sequence[sequence.count - 1], sequence[sequence.count - 2])
            if didOveflow == true {
                println("overFlow happened")
                break
            }
            sequence.append(nextNumber)
        }
        sequence.removeLast()
        values = sequence
    }
}

let fibanocciSequence = FibonacciSequence(maxNumber:5000000000000, includesZero: false)
let anotherSequence = FibonacciSequence(numberOfItemsInSequence: 130, includesZero: true)
println(fibanocciSequence.values)
//end
